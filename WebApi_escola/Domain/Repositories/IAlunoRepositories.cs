﻿using WebApi_escola.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi_escola.Domain.Repositories
{
    public interface IAlunoRepositories : IRepository<Aluno>
    {
        /// <summary>
        /// Obtem um aluno pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Aluno FindById(int id);
    }
}
