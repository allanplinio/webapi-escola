﻿using System.Collections.Generic;


namespace WebApi_escola.Domain.Repositories
{
    public interface IRepository<T>
    {
        /// <summary>
        /// Método genérico para adicionar um novo registro
        /// </summary>
        /// <param name="entity"></param>
        void Add(T entity);
        /// <summary>
        /// Método genérico para carregar todos os dados de uma entidade
        /// </summary>
        /// <returns></returns>
        List<T> GetAll();
        /// <summary>
        /// Método genérico para remover um registro do banco
        /// </summary>
        /// <param name="entity"></param>
        void Delete(T entity);
        /// <summary>
        /// Método genérico para commitar uma transação
        /// </summary>
        void SalveChanges();
    }
}
