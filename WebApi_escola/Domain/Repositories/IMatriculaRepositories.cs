﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Repositories
{
    public interface IMatriculaRepositories : IRepository<Matricula>
    {
        /// <summary>
        /// Obtem uma matricula pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Matricula FindById(int id);
    }
}
