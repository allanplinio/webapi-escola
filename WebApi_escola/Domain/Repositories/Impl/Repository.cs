﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApi_escola.Domain.Repositories.Impl;

namespace WebApi_escola.Domain.Repositories.Impl
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly DataContext _dataContext;

        public Repository(DataContext dataContext)
        {
            _dataContext = dataContext;            
        }

        public DataContext DataContext
        {
            get { return _dataContext; }
        }

        public void Add(T entity)
        {
            _dataContext.Set<T>().Add(entity);
            _dataContext.SaveChanges();
        }

        public List<T> GetAll()
        {
            return _dataContext.Set<T>().ToList();
        }
        
        public void Delete(T entity)
        {
            _dataContext.Set<T>().Remove(entity);
            _dataContext.SaveChanges();
        }

        public void SalveChanges()
        {
            _dataContext.SaveChanges();
        }
    }
}
