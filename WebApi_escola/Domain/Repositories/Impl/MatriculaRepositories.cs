﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Repositories.Impl
{
    public class MatriculaRepositories : Repository<Matricula>, IMatriculaRepositories
    {
        public MatriculaRepositories(DataContext dataContext) : base(dataContext)
        {
        }

        public Matricula FindById(int id)
        {
            return this.DataContext.Matriculas.FirstOrDefault(c => c.Id == id);
        }

    }
}
