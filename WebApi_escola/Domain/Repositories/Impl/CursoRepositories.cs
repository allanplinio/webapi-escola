﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Repositories.Impl
{
    public class CursoRepositories : Repository<Curso>, ICursoRepositories
    {
        public CursoRepositories(DataContext dataContext) : base(dataContext)
        {
        }

        public Curso FindById(int id)
        {
            return this.DataContext.Cursos.FirstOrDefault(c => c.Id == id);
        }
    }
}
