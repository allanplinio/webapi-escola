﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Repositories.Impl
{
    public class ProfessorRepositories : Repository<Professor>, IProfessorRepositories
    {
        public ProfessorRepositories(DataContext dataContext) : base(dataContext)
        {
        }

        public Professor FindById(int id)
        {
            return this.DataContext.Professores.FirstOrDefault(c => c.Id == id);
        }
    }
}
