﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Repositories.Impl
{
    public class AlunoRepositories : Repository<Aluno>, IAlunoRepositories
    {
        public AlunoRepositories(DataContext dataContext) : base(dataContext)
        {

        }

        public Aluno FindById(int id)
        {
            return this.DataContext.Alunos.FirstOrDefault(c => c.Id == id);
        }
    }
}
