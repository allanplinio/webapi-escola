﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Repositories.Impl
{
    public class TurmaRepositories : Repository<Turma>, ITurmaRepositories
    {
        public TurmaRepositories(DataContext dataContext) : base(dataContext)
        {
        }

        public Turma FindById(int id)
        {
            return this.DataContext.Turmas.FirstOrDefault(c => c.Id == id);
        }

        public Turma ObterTurmaPorCurso(int idCurso)
        {
            return this.DataContext.Turmas.FirstOrDefault(c => c.IdCurso == idCurso);
        }
    }
}
