﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Repositories
{
    public interface IProfessorRepositories: IRepository<Professor>
    {
        /// <summary>
        /// Obtem um professor pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Professor FindById(int id);
    }
}
