﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Repositories
{
    public interface ICursoRepositories: IRepository<Curso>
    {
        /// <summary>
        /// Obtem um curo pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Curso FindById(int id);
    }
}
