﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Repositories
{
    public interface ITurmaRepositories : IRepository<Turma>
    {
        /// <summary>
        /// Obterm uma turma pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Turma FindById(int id);
        /// <summary>
        /// Obtem uma turma pelo ID do curso
        /// </summary>
        /// <param name="idCurso"></param>
        /// <returns></returns>
        Turma ObterTurmaPorCurso(int idCurso);
    }
}
