﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi_escola.Domain.Dto
{
    public class MatriculaDTO
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string DataNascimento { get; set; }

        public string Cpf { get; set; }

        public string Rg { get; set; }

        public string Cep { get; set; }

        public string Cidade { get; set; }

        public string Logradouro { get; set; }

        public string Bairro { get; set; }

        public string Numero { get; set; }

        public string Celular { get; set; }

        public string Email { get; set; }
        public string NomeResponsavel { get; set; }

        public int IdCurso { get; set; }

    }
}
