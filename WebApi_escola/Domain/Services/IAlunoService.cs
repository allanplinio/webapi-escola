﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Services
{
    public interface IAlunoService
    {
        /// <summary>
        /// Salva um aluno no banco de dados
        /// </summary>
        /// <param name="aluno"></param>
        void Salvar(Aluno aluno);
        /// <summary>
        /// Realiza uma alteração de um aluno
        /// </summary>
        /// <param name="aluno"></param>
        void Alterar(Aluno aluno);
        /// <summary>
        /// Obterm todos os alunos
        /// </summary>
        /// <returns></returns>
        List<Aluno> ObterAlunos();
        /// <summary>
        /// Obterm um aluno pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Aluno ObterAluno(int id);
        /// <summary>
        /// Remover um aluno pelo ID
        /// </summary>
        /// <param name="id"></param>
        void Deletar(int id);
    }
}
