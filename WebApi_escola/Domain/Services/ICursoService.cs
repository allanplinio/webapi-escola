﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Services
{
    public interface ICursoService
    {
        /// <summary>
        /// Salva as um novo curso no banco de dados
        /// </summary>
        /// <param name="curso"></param>
        void Salvar(Curso curso);
        /// <summary>
        /// Altera as informações de um curso
        /// </summary>
        /// <param name="curso"></param>
        void Alterar(Curso curso);
        /// <summary>
        /// Obterm todos os cursos
        /// </summary>
        /// <returns></returns>
        List<Curso> ObterCursos();
        /// <summary>
        /// Obterm um curso pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Curso ObterCurso(int id);
        /// <summary>
        /// Remover um curso pelo ID
        /// </summary>
        /// <param name="id"></param>
        void Deletar(int id);
    }
}
