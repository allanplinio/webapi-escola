﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Dto;
using WebApi_escola.Domain.Entities;
using WebApi_escola.Domain.Repositories;

namespace WebApi_escola.Domain.Services.Impl
{
    public class MatriculaService : IMatriculaService
    {
        private IMatriculaRepositories _matriculaRepositories;
        private IAlunoService _alunoService;
        private ITurmaRepositories _turmaRepositories;

        public MatriculaService(IMatriculaRepositories matriculaRepositories,
                                IAlunoService alunoService,
                                ITurmaRepositories turmaRepositories)
        {
            _matriculaRepositories = matriculaRepositories;
            _alunoService = alunoService;
            _turmaRepositories = turmaRepositories;
        }

        public void Salvar(MatriculaDTO matriculaDTO )
        {
            DateTime dataNascimento;
            var isData = DateTime.TryParse(matriculaDTO.DataNascimento, out dataNascimento);

            var aluno = new Aluno
            {
                Nome = matriculaDTO.Nome,
                Bairro = matriculaDTO.Bairro,
                Celular = matriculaDTO.Celular,
                Cep = matriculaDTO.Cep,
                Cidade = matriculaDTO.Cidade,
                Cpf = matriculaDTO.Cpf,
                DataNascimento = dataNascimento,
                Email = matriculaDTO.Email,
                Logradouro = matriculaDTO.Logradouro,
                NomeResponsavel = matriculaDTO.NomeResponsavel,
                Numero = matriculaDTO.Numero,
                Rg = matriculaDTO.Rg
            };

            var matricula = new Matricula
            {
                Aluno = aluno,
                Aprovado = false,
                Cancelado = false
            };


            var turma = ObterTurma(matriculaDTO.IdCurso);
            matricula.IdTurma = turma.Id;

            _matriculaRepositories.Add(matricula);
            _matriculaRepositories.SalveChanges();
        }


        public Turma ObterTurma(int idCurso)
        {
            var turma = _turmaRepositories.ObterTurmaPorCurso(idCurso);
            if (turma != null) return turma;

            turma = new Turma { IdCurso = idCurso };

            _turmaRepositories.Add(turma);
            return turma;
        }
    }
}
