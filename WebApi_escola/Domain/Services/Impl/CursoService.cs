﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;
using WebApi_escola.Domain.Repositories;

namespace WebApi_escola.Domain.Services.Impl
{
    public class CursoService : ICursoService
    {
        private readonly ICursoRepositories _cursoRepositories;

        public CursoService(ICursoRepositories cursoRepositories)
        {
            _cursoRepositories = cursoRepositories;
        }

        public void Alterar(Curso curso)
        {
            var cursoDb = _cursoRepositories.FindById(curso.Id);
            cursoDb.Nome = curso.Nome;
            cursoDb.Valor = curso.Valor;
            cursoDb.Descricao = curso.Descricao;

            _cursoRepositories.SalveChanges();
        }

        public void Deletar(int id)
        {
            var curso = _cursoRepositories.FindById(id);
            _cursoRepositories.Delete(curso);
        }

        public Curso ObterCurso(int id)
        {
            return  _cursoRepositories.FindById(id);
        }

        public List<Curso> ObterCursos()
        {
            return _cursoRepositories.GetAll();
        }

        public void Salvar(Curso curso)
        {
            _cursoRepositories.Add(curso);
        }
    }
}
