﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;
using WebApi_escola.Domain.Repositories;

namespace WebApi_escola.Domain.Services.Impl
{
    public class AlunoService : IAlunoService
    {
        private readonly IAlunoRepositories _alunoRepositories;

        public AlunoService(IAlunoRepositories alunoRepositories)
        {
            _alunoRepositories = alunoRepositories;
        }

        public void Salvar(Aluno aluno)
        {
            _alunoRepositories.Add(aluno);
        }

        public void Alterar(Aluno aluno)
        {
            var alunoDb = _alunoRepositories.FindById(aluno.Id);
            alunoDb.Nome = aluno.Nome;
            alunoDb.Bairro = aluno.Bairro;
            alunoDb.Celular = aluno.Celular;
            alunoDb.Cep = aluno.Cep;
            alunoDb.Cidade = aluno.Cidade;
            alunoDb.Cpf = aluno.Cpf;
            alunoDb.DataNascimento = aluno.DataNascimento;
            alunoDb.Email = aluno.Email;
            alunoDb.Logradouro = aluno.Logradouro;
            alunoDb.Matriculas = aluno.Matriculas;
            alunoDb.NomeResponsavel = aluno.NomeResponsavel;
            alunoDb.Numero = aluno.Numero;
            alunoDb.Rg = aluno.Rg;

            _alunoRepositories.SalveChanges();
        }

        public void Deletar(int id)
        {
            var aluno = _alunoRepositories.FindById(id);
            _alunoRepositories.Delete(aluno);
        }

        public Aluno ObterAluno(int id)
        {
            return _alunoRepositories.FindById(id);
        }

        public List<Aluno> ObterAlunos()
        {
            return _alunoRepositories.GetAll();
        }

    }
}
