﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;
using WebApi_escola.Domain.Repositories;

namespace WebApi_escola.Domain.Services.Impl
{
    public class ProfessorService : IProfessorService
    {
        private readonly IProfessorRepositories _professorRepositories;

        public ProfessorService(IProfessorRepositories professorRepositories)
        {
            _professorRepositories = professorRepositories;
        }

        public void Alterar(Professor professor)
        {
            var professorDb = _professorRepositories.FindById(professor.Id);
            professorDb.Nome = professor.Nome;
            professorDb.Celular = professor.Celular;
            professorDb.Cpf = professor.Cpf;
            professorDb.Email = professor.Email;
            professorDb.Rg = professor.Rg;
            professorDb.Escolaridade = professor.Escolaridade;
            professorDb.Formacao = professor.Formacao;

            _professorRepositories.SalveChanges();
        }

        public void Deletar(int id)
        {
            var professor = _professorRepositories.FindById(id);
            _professorRepositories.Delete(professor);
        }

        public Professor ObterProfessor(int id)
        {
            return  _professorRepositories.FindById(id);
        }

        public List<Professor> ObterProfessores()
        {
            return _professorRepositories.GetAll();
        }

        public void Salvar(Professor professor)
        {
            _professorRepositories.Add(professor);
        }
    }
}
