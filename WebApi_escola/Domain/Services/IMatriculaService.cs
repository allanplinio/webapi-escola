﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Dto;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Services
{
    public interface IMatriculaService
    {
        /// <summary>
        /// Salva uma nova matricula
        /// </summary>
        /// <param name="matricula"></param>
        void Salvar(MatriculaDTO matricula);
    }
}
