﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;

namespace WebApi_escola.Domain.Services
{
    public interface IProfessorService
    {
        /// <summary>
        /// Salvar um novo professor
        /// </summary>
        /// <param name="professor"></param>
        void Salvar(Professor professor);
        /// <summary>
        /// Alterar as informações de um professor
        /// </summary>
        /// <param name="professor"></param>
        void Alterar(Professor professor);
        /// <summary>
        /// Obtem todos os professores
        /// </summary>
        /// <returns></returns>
        List<Professor> ObterProfessores();
        /// <summary>
        /// Obterm um professor pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Professor ObterProfessor(int id);
        /// <summary>
        /// Deleta um professor pelo ID
        /// </summary>
        /// <param name="id"></param>
        void Deletar(int id);
    }
}
