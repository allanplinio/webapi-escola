﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi_escola.Domain.Entities
{
    [Table("turma")]
    public class Turma
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("idturma")]
        public int Id { get; set; }

        [Column("idcurso")]
        [Required]
        public int IdCurso { get; set; }

        [Column("idprofessor")]
        public int? IdProfessor { get; set; }

        [Column("dateinicio")]
        public DateTime? DataInicio { get; set; }

        [Column("datafim")]
        public DateTime? DataFim { get; set; }

        [Column("turno")]
        public string Turno { get; set; }

    }
}
