﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi_escola.Domain.Entities
{
    [Table("aluno")]
    public class Aluno
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("idaluno")]
        public int Id { get; set; }

        [Column("nome")]
        [Required]
        [MaxLength(100, ErrorMessage = "Maximo de caracteres e 100")]
        public string Nome { get; set; }

        [Column("datanascimento")]
        [Required]
        public DateTime DataNascimento { get; set; }

        [Column("cpf")]
        [MaxLength(11, ErrorMessage = "Maximo de caracteres e 11")]
        public string Cpf { get; set; }

        [Column("rg")]
        [MaxLength(10, ErrorMessage = "Maximo de caracteres e 10")]
        public string Rg { get; set; }

        [Column("cep")]
        [MaxLength(8, ErrorMessage = "Maximo de caracteres e 8")]
        public string Cep { get; set; }

        [Column("cidade")]
        [MaxLength(200, ErrorMessage = "Maximo de caracteres e 200")]
        public string Cidade { get; set; }

        [Column("logradouro")]
        [MaxLength(255, ErrorMessage = "Maximo de caracteres e 255")]
        public string Logradouro { get; set; }

        [Column("bairro")]
        [MaxLength(150, ErrorMessage = "Maximo de caracteres e 150")]
        public string Bairro { get; set; }

        [Column("numero")]
        [MaxLength(45, ErrorMessage = "Maximo de caracteres e 45")]
        public string Numero { get; set; }

        [Column("celular")]
        [MaxLength(11, ErrorMessage = "Maximo de caracteres e 11")]
        public string Celular { get; set; }

        [Column("email")]
        [MaxLength(200, ErrorMessage = "Maximo de caracteres e 200")]
        public string Email { get; set; }

        [Column("nomeresponsavel")]
        [MaxLength(200, ErrorMessage = "Maximo de caracteres e 200")]
        public string NomeResponsavel { get; set; }

        //[InverseProperty("idmatricula")]
        public List<Matricula> Matriculas { get; set; }

        public Aluno()
        {
            Matriculas = new List<Matricula>();
        }
    }
}
