﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi_escola.Domain.Entities
{
    [Table("professor")]
    public class Professor
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("idprofessor")]
        public int Id { get; set; }

        [Column("nome")]
        [Required]

        public string Nome { get; set; }

        [Column("cpf")]
        public string Cpf { get; set; }

        [Column("rg")]
        public string Rg { get; set; }

        [Column("celular")]
        public string Celular { get; set; }

        [Column("email")]
        public string Email { get; set; }

        [Column("escolaridade")]
        public string Escolaridade { get; set; }

        [Column("formacao")]
        public string Formacao { get; set; }
    }
}
