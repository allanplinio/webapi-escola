﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi_escola.Domain.Entities
{
    [Table("curso")]
    public class Curso
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("idcurso")]
        public int Id { get; set; }
        [Column("nome")]
        [Required]
        [MaxLength(200, ErrorMessage = "Maximo de caracteres e 200")]
        public string Nome { get; set; }
        [Column("valor")]
        public double Valor { get; set; }
        [Column("descricao")]
        public string Descricao { get; set; }
    }
}
