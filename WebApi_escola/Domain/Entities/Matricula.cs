﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi_escola.Domain.Entities
{
    [Table("matricula")]
    public class Matricula
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("idmatricula")]
        public int Id { get; set; }

        [ForeignKey("Aluno")]
        [Column("idaluno")]
        public int IdAluno { get; set; }

        [Column("idturma")]
        public int IdTurma{ get; set; }

        [Column("valormatricula")]
        public double ValorMatricula { get; set; }

        [Column("valorcurso")]
        public double ValorCurso { get; set; }

        [Column("cancelada")]
        public bool Cancelado { get; set; }

        [Column("aprovado")]
        public bool Aprovado { get; set; }
        
        public Aluno Aluno { get; set; }

        public Matricula()
        {

        }
    }
}
