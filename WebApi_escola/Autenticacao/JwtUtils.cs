﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace WebApi_escola.Autenticacao
{
    public static class JwtUtils
    {
        public static void AddMvcWithPolicy(this IServiceCollection services)
        {
            services.AddMvc(config =>
            {
                AuthorizationPolicy policy = new AuthorizationPolicyBuilder()
                                .RequireAuthenticatedUser()
                                .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("UserApi", policy => policy.RequireClaim("Auth", "WebApi"));
            });

        }

        public static void AddJwtOptions(this IServiceCollection services, IConfiguration configuration,
            SymmetricSecurityKey signingKey)
        {
            //IConfigurationSection jwtAppSettingOptions = configuration.GetSection(nameof(JwtIssuerOptions));
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = configuration["JwtIssuerOptions:Issuer"];//jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = configuration["JwtIssuerOptions:Audience"];// jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            });
        }

        public static void UseJwtBearerAuthenticationWithOptions(this IApplicationBuilder app, IConfiguration configuration, SymmetricSecurityKey signingKey)
        {            
            //IConfigurationSection jwtAppSettingOptions = configuration.GetSection("JwtIssuerOptions"); //configuration.GetSection(nameof(JwtIssuerOptions));
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = configuration["JwtIssuerOptions:Issuer"], //jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = configuration["JwtIssuerOptions:Audience"],//jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,

                RequireExpirationTime = true,
                ValidateLifetime = true,

                ClockSkew = TimeSpan.Zero
            };

            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters
            });
        }

        public static void UseRequestLocalizationFromBrazil(this IApplicationBuilder app)
        {
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("pt-BR", "pt-BR")
            });
        }

        public static long ToUnixEpochDate(this DateTime date)
        {
            return (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
        }

        public static string ToUnixEpochDateToString(this DateTime date)
        {
            return $"{ToUnixEpochDate(date)}";
        }

        public class JwtIssuerOptions
        {
            public string Issuer { get; set; }
            public string Audience { get; set; }
            public SigningCredentials SigningCredentials { get; set; }
            public TimeSpan ValidFor { get; set; } = TimeSpan.FromMinutes(10);
            public DateTime NotBefore { get; set; }
            public DateTime Expiration { get; set; } = DateTime.Now.AddHours(1);
            

            public JwtIssuerOptions()
            {
                Issuer = "WebApi Sistema Escolar";
                Audience = "http://localhost:59968/";
            }
        }
    }
}
