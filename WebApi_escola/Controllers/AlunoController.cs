﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;
using WebApi_escola.Domain.Repositories;
using WebApi_escola.Domain.Services;

namespace WebApi_escola.Controllers
{
    [Route("api/[controller]")]
    public class AlunoController : Controller
    {

        private readonly IAlunoService _alunoService;

        public AlunoController(IAlunoService alunoService)
        {
            _alunoService = alunoService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_alunoService.ObterAlunos().ToArray());
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Ok(_alunoService.ObterAluno(id));
        }

        [HttpPost]
        public void Post([FromBody]Aluno aluno)
        {
            if (aluno != null)
            {
                _alunoService.Salvar(aluno);
            }
            else
                NotFound();
        }

        [HttpPut]
        public void Put([FromBody]Aluno aluno)
        {
            if(aluno != null)
            {
                _alunoService.Alterar(aluno);
            }
            else
                NotFound();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            if(id >0)
                _alunoService.Deletar(id);
            else
                NotFound();
        }
    }
}
