﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;
using WebApi_escola.Domain.Repositories;
using WebApi_escola.Domain.Services;

namespace WebApi_escola.Controllers
{
    [Route("api/[controller]")]
    public class CursoController : Controller
    {

        private readonly ICursoService _cursoService;

        public CursoController(ICursoService cursoService)
        {
            _cursoService = cursoService;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetAll()
        {
            return Ok(_cursoService.ObterCursos().ToArray());
        }

        [HttpGet("{id}")]
        public Curso GetById(int id)
        {
            return _cursoService.ObterCurso(id);
        }

        [HttpPost]
        public void Post([FromBody]Curso curso)
        {
            if (curso != null)
            {
                _cursoService.Salvar(curso);
            }
            else
                NotFound();
        }

        [HttpPut]
        public void Put([FromBody]Curso curso)
        {
            if(curso != null)
            {
                _cursoService.Alterar(curso);
            }
            else
                NotFound();
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            if(id >0)
                _cursoService.Deletar(id);
            else
                NotFound();
        }
    }
}
