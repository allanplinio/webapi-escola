﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_escola.Domain.Entities;
using WebApi_escola.Domain.Repositories;
using WebApi_escola.Domain.Services;

namespace WebApi_escola.Controllers
{
    [Route("api/[controller]")]
    public class ProfessorController : Controller
    {
        private readonly IProfessorService _professorService;

        public ProfessorController(IProfessorService professorService)
        {
            _professorService = professorService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_professorService.ObterProfessores().ToArray());
        }

        [HttpGet("{id}")]
        public Professor GetById(int id)
        {
            return _professorService.ObterProfessor(id);
        }

        [HttpPost]
        public void Post([FromBody]Professor professor)
        {
            if (professor != null)
            {
                _professorService.Salvar(professor);
            }
            else
                NotFound();
        }

        [HttpPut]
        public void Put([FromBody]Professor professor)
        {
            if(professor != null)
            {
                _professorService.Alterar(professor);
            }
            else
                NotFound();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            if(id >0)
                _professorService.Deletar(id);
            else
                NotFound();
        }
    }
}
