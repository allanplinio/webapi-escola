﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebApi_escola.Domain.Dto;
using WebApi_escola.Domain.Entities;
using WebApi_escola.Domain.Repositories;
using WebApi_escola.Domain.Services;

namespace WebApi_escola.Controllers
{
    [Route("api/[controller]")]
    public class MatriculaController : Controller
    {
        private readonly IMatriculaService _matriculaService;

        public MatriculaController(IMatriculaService matriculaService)
        {
            _matriculaService = matriculaService;
        }
       

        [HttpPost]
        [AllowAnonymous]
        public void Post([FromBody]MatriculaDTO matricula)
        {

            if (matricula != null)
            {
                _matriculaService.Salvar(matricula);
            }
        }
    }
}
