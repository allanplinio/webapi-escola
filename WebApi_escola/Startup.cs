﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using WebApi_escola.Domain.Services.Impl;
using WebApi_escola.Domain.Services;
using Microsoft.EntityFrameworkCore;
using WebApi_escola.Domain.Repositories.Impl;
using WebApi_escola.Domain.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using WebApi_escola.Autenticacao;

namespace WebApi_escola
{
    public class Startup
    {
        public const string SecretKey = "a1234567891012141516182025262b";
        public readonly SymmetricSecurityKey SigningKey =
            new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();            
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    b => b.WithOrigins("http://localhost:4200", "http://localhost:59968/api/")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            var connectionString = Configuration["dbContextSettings:ConnectionString"];
            services.AddDbContext<DataContext>(opt => opt.UseNpgsql(connectionString));


            services.AddMvc()
                    .AddJsonOptions(a =>
                        a.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver()); ;


            services.AddMvcWithPolicy();
            services.AddJwtOptions(Configuration, SigningKey);


            services.AddScoped<IAlunoService, AlunoService>();
            services.AddScoped<IAlunoRepositories, AlunoRepositories>();
            services.AddScoped<IProfessorService, ProfessorService>();
            services.AddScoped<IProfessorRepositories, ProfessorRepositories>();
            services.AddScoped<ICursoService, CursoService>();
            services.AddScoped<ICursoRepositories, CursoRepositories>();
            services.AddScoped<IMatriculaService, MatriculaService>();
            services.AddScoped<IMatriculaRepositories, MatriculaRepositories>();
            services.AddScoped<ITurmaRepositories, TurmaRepositories>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            app.UseRequestLocalizationFromBrazil();
            app.UseJwtBearerAuthenticationWithOptions(Configuration, SigningKey);
            app.UseCors("CorsPolicy");
            app.UseMvc();
        }
    }
}
